/**
 * @file main.cpp
 * @author Michael Wolkstein (mw@3rd-element.com)
 * @brief Simple PWM Generator
 * @version 1.1
 * @date 06.05.2021
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <Arduino.h>
#include <Servo.h>
#include <elapsedMillis.h>

// global objects
elapsedMillis _O_blinkLED;
elapsedMillis _O_ServoUp;

Servo _O_Servo;

// global Vars
uint16_t __blinkTime;
uint16_t __servoStart = 900;
uint16_t __dreiBlink = 1500;

void setup() {
  Serial.begin(9600);
  // Set Global Vars
  __blinkTime = 250; //0.5 seconds
  // LED BUILDIN Active
  pinMode(LED_BUILTIN ,OUTPUT);
  
  // attach Servo Pin 
  _O_Servo.attach(3);
  // wirte 1400us to Servo
  //_O_Servo.writeMicroseconds(__servoStart);
  _O_Servo.writeMicroseconds(__dreiBlink);

}

void loop() {
  // LED Blink
  if(_O_blinkLED >= __blinkTime){
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    _O_blinkLED = 0;
    _O_Servo.writeMicroseconds(__dreiBlink);
  }

  // pwm test
  // if(_O_ServoUp > 5000){
  //   __servoStart += 50;
  //   if(__servoStart >= 2100) __servoStart = 900;
  //   //_O_Servo.writeMicroseconds(__servoStart);
  //   Serial.println(__servoStart);
  //   _O_ServoUp = 0;
  // }
   
}
